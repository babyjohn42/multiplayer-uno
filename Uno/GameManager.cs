﻿using System.Collections.Generic;
using Uno.Cards;

namespace Uno
{
    public class GameManager
    {
        public Deck DrawPile { get; set; }
        public Deck DiscardPile { get; set; }
        public List<Player> Players { get; set; }


        public Player Dealer { get; set; }
        public Player CurrentPlayer { get; set; }

        private int nextPlayerDraws = 0;
        private bool nextPlayerIsSkipped = false;
        private bool turnOrderIsClockwise = true;
        private bool someoneHasWon = false;
        
        public void NextPlayerDraws(int numberOfCards)
        {
            Skip();
            nextPlayerDraws = numberOfCards;
        }

        public void NextPlayer(bool verbose = true)
        {
            if (CurrentPlayer == null)
            {
                CurrentPlayer = Dealer;
            }
            else if (verbose)
            {
                UnoConsole.WriteLine($"End of {CurrentPlayer.Name}'s turn.");
            }

            int index = Players.IndexOf(CurrentPlayer);

            if (turnOrderIsClockwise)
            {
                index = (index + 1) % Players.Count;
            }
            else
            {
                index = (index + Players.Count - 1) % Players.Count; 
            }

            CurrentPlayer = Players[index];
        }

        public void Skip()
        {
            nextPlayerIsSkipped = true;
        }

        public void ReverseOrder()
        {
            UnoConsole.WriteLine("Play is reversed!");
            turnOrderIsClockwise = !turnOrderIsClockwise;
        }

        public void PlayerDraws(int numOfCards, bool verbose = true)
        {
            if (verbose)
            {
                UnoConsole.WriteLine($"{CurrentPlayer.Name} draws {numOfCards} cards!");
            }

            for (int i=0; i<numOfCards; i++)
            {
                PlayerDraws1Card(out Card cardDrawn, verbose);
            }
        }

        private void PlayerDraws1Card(out Card cardDrawn, bool verbose = true)
        {
            cardDrawn = DrawPile.Draw();
            CurrentPlayer.Draw(cardDrawn);
            if (verbose)
            {
                UnoConsole.WriteLine($"You drew a {cardDrawn}.", cardDrawn.Color);
            }

            if (DrawPile.IsEmpty)
            {
                Card topCard = DiscardPile.Draw();

                // reshuffle discard pile into draw pile
                UnoConsole.WriteLine("Reshuffling...");
                DrawPile = DiscardPile;
                DrawPile.IsFaceUp = false;
                DrawPile.Shuffle();

                // new discard pile from the previous top card
                DiscardPile = new Deck(true);
                DiscardPile.Discard(topCard);
            }
        }

        public void StartTurn()
        {
            UnoConsole.WriteWildLine("----------------------------------------------------------");
            UnoConsole.WriteLine($"Start of {CurrentPlayer.Name}'s turn.");
            CurrentPlayer.StartTurn();
            UserChoice();
            EndTurn();
        }

        public void EndTurn(bool verbose = false)
        {
            if (!someoneHasWon)
            {
                NextPlayer(verbose);
                if (nextPlayerIsSkipped)
                {
                    if (nextPlayerDraws > 0)
                    {
                        PlayerDraws(nextPlayerDraws);
                        nextPlayerDraws = 0;
                    }

                    UnoConsole.WriteLine($"{CurrentPlayer.Name}'s turn is skipped!");
                    NextPlayer();
                    nextPlayerIsSkipped = false;
                }

                StartTurn();
            }
        }

        public void UserChoice()
        {
            while (true)
            {
                DisplayTable();
                UnoConsole.Write($"{CurrentPlayer.Name}, what would you like to do? ");
                string input = UnoConsole.ReadLine();

                if (CurrentPlayer.CanPerform(ref input))
                {
                    switch(input?.ToLower()?.Trim())
                    {
                        case "play":
                            PlayCard();
                            if (someoneHasWon)
                            {
                                return;
                            }
                            break;

                        case "uno":
                            CurrentPlayer.AnnounceUno();
                            break;

                        case "draw":
                            PlayerDraws1Card(out Card drawnCard);
                            Card topCard = DiscardPile.Peek();

                            if (drawnCard.CanBePlayedOn(topCard))
                            {
                                if ((drawnCard is WildDraw4Card
                                    && CurrentPlayer.CanPlayWildDraw4(topCard))
                                    || !(drawnCard is WildDraw4Card))
                                {
                                    PlayCard(drawnCard);
                                    if (someoneHasWon)
                                    {
                                        return;
                                    }
                                }
                            }
                            break;

                        case "end":
                            if (!CurrentPlayer.SaidUno && CurrentPlayer.HandCount == 1)
                            {
                                UnoConsole.WriteLine("You didn't say UNO!");
                                PlayerDraws(2);
                            }
                            return;
                    }
                }
                else
                {
                    UnoConsole.WriteErrorLine("Please enter a valid action.");
                }
            }
        }

        private void PlayCard(Card card)
        {
            CurrentPlayer.Play(card);
            DiscardPile.Discard(card);

            UnoConsole.WriteLine($"You played a {card}.", card.Color);

            if (CurrentPlayer.HandCount == 0)
            {
                someoneHasWon = true;
                UnoConsole.WriteWildLine($"{CurrentPlayer.Name} has won!");
            }

            if (card is ActionCard actionCard)
            {
                actionCard.EffectFromHand(this);
            }
        }

        private void PlayCard()
        {
            Card topCard = DiscardPile.Peek();

            if (CurrentPlayer.CannotPlayAnyCards(topCard))
            {
                UnoConsole.WriteErrorLine("You don't have any cards you can play right now.");
                return;
            }

            UnoConsole.WriteLine();
            UnoConsole.Write("Discard pile: ");
            UnoConsole.WriteLine(topCard, topCard.Color);
            UnoConsole.WriteLine();
            CurrentPlayer.DisplayHand();

            UnoConsole.Write($"Choose a card to play (1-{CurrentPlayer.HandCount}): ");

            int cardChoice = UnoConsole.ReadIntInRange(1, CurrentPlayer.HandCount);

            Card choice = CurrentPlayer.GetCard(cardChoice);
            Card topDiscard = DiscardPile.Peek();

            if (choice is WildDraw4Card && !CurrentPlayer.CanPlayWildDraw4(topDiscard))
            {
                UnoConsole.WriteErrorLine("You can't play a Wild Draw 4 card right now!");
            }
            else if (choice.CanBePlayedOn(topDiscard))
            {
                PlayCard(choice);
            }
            else
            {
                UnoConsole.WriteErrorLine("You can't play that card right now.");
            }
        }

        private void DisplayTable()
        {
            UnoConsole.WriteLine();
            foreach (Player player in Players)
            {
                UnoConsole.WriteLine($"{player.Name} has {player.HandCount} cards left.");
            }

            UnoConsole.WriteLine();
            UnoConsole.Write($"Discard pile: ");

            Card topCard = DiscardPile.Peek();
            UnoConsole.WriteLine(topCard, topCard.Color);
            UnoConsole.WriteLine();

            CurrentPlayer.DisplayHand();

            UnoConsole.WriteLine();
        }
    }
}
