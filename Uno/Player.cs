﻿using System;
using System.Collections.Generic;
using Uno.Cards;

namespace Uno
{
    public class Player
    {
        public int HandCount { get => hand.Count; }

        private List<Card> hand = new List<Card>();
        public string Name { get; set; }
        public bool SaidUno { get; private set; } = false;
        private bool DrawnCard = false;
        private bool PlayedCard = false;

        public Player(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException();
            }

            Name = name;
        }

        public void StartTurn()
        {
            SaidUno = DrawnCard = PlayedCard = false;
        }

        public void Draw(Card card)
        {
            // preserves hand order
            int index = hand.BinarySearch(card);
            
            hand.Insert(index >= 0? index : ~index, card);

            DrawnCard = true;
            SaidUno = false;
        }

        public void Play(Card card)
        {
            hand.Remove(card);

            PlayedCard = true;
        }

        public bool CanPerform(ref string input)
        {
            switch(input?.ToLower()?.Trim())
            {
                case null:
                    return false;

                case "play":
                case "play card":
                case "discard":
                case "discard card":
                    input = "play";
                    return !(PlayedCard || DrawnCard);

                case "draw":
                case "draw card":
                    input = "draw";
                    return !(PlayedCard || DrawnCard);

                case "uno":
                case "say uno":
                case "uno!":
                    input = "uno";
                    return (HandCount == 1);

                case "end":
                case "end turn":
                case "done":
                case "next":
                    input = "end";
                    return (PlayedCard || DrawnCard);

                default:
                    return false;
            }

        }

        public Card GetCard(int cardChoice)
        {
            if (cardChoice < 1 || cardChoice > HandCount)
            {
                throw new ArgumentOutOfRangeException();
            }

            return hand[cardChoice - 1];
        }

        public bool CanPlayWildDraw4(Card topCard)
        {
            string play = "play";
            if (!CanPerform(ref play))
            {
                return false;
            }

            foreach (Card card in hand)
            {
                if (!(card is WildDraw4Card)
                    && card.CanBePlayedOn(topCard))
                {
                    return false;
                }
            }

            return true;
        }

        public void AnnounceUno()
        {
            
            UnoConsole.WriteWildLine("UNO!");
            SaidUno = true;
        }

        public void DisplayHand()
        {
            for (int i=0; i<hand.Count; i++)
            {
                Card card = hand[i];
                UnoConsole.Write($"{i + 1} - ");
                UnoConsole.WriteLine(card, card.Color);
            }
        }

        public bool CannotPlayAnyCards(Card topCard)
        {
            foreach(Card card in hand)
            {
                if (card.CanBePlayedOn(topCard))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
