﻿using System;

namespace Uno
{
    class Program
    {
        static void Main(string[] args)
        {
            GameInitializer initializer = new GameInitializer();
            initializer.Initialize();

            UnoConsole.WriteWildLine("Thanks for playing UNO!");
            UnoConsole.ReadLine();
        }
    }
}
