﻿using System;
using Uno.Cards;

namespace Uno
{
    public class UnoDrawPileBuilder
    {
        private bool deckIsReady = false;
        private Deck deck;

        public Deck Deck
        {
            get => deckIsReady ? deck : null;
        }

        public void Build()
        {
            deck = new Deck(false);

            foreach (Color color in Enum.GetValues(typeof(Color)))
            {
                if (color == Color.Wild)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        deck.Discard(new WildCard());
                        deck.Discard(new WildDraw4Card());
                    }
                }
                else
                {
                    // set up color cards
                    deck.Discard(new Card(color, "0"));

                    for (int i=0; i<1; i++)
                    {
                        for (int j=1; j<=9; j++)
                        {
                            deck.Discard(new Card(color, j.ToString()));
                        }

                        deck.Discard(new Draw2Card(color));
                        deck.Discard(new ReverseCard(color));
                        deck.Discard(new SkipCard(color));
                    }
                }
            }

            deck.Shuffle();

            deckIsReady = true;
        }
    }
}
