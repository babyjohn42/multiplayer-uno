﻿using System;

namespace Uno
{
    public static class UnoConsole
    {
        private static void ResetColor()
        {
            Console.ResetColor();
        }

        private static void ChangeColor(ConsoleColor fg, ConsoleColor bg)
        {
            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;
        }

        private static void ChangeColor(Color fg)
        {
            switch (fg)
            {
                case Color.Red:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;

                case Color.Green:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;

                case Color.Blue:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;

                case Color.Yellow:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
            }
        }

        public static void Write(object output = null, Color? fg = null)
        {
            if (output == null)
            {
                return;
            }

            if (fg != null)
            {
                if (fg == Color.Wild)
                {
                    WriteWild(output);
                    return;
                }
                else
                {
                    ChangeColor((Color)fg);
                }
            }

            Console.Write(output);
            ResetColor();
        }

        private static void WriteWild(object output = null)
        {
            if (output == null)
            {
                return;
            }

            Color[] colors = { Color.Red, Color.Blue, Color.Yellow, Color.Green };
            
            int colorIndex = 0;
            Color currentColor = colors[colorIndex];

            string outputString = output.ToString();
            foreach (char letter in outputString)
            {
                // change color
                ChangeColor(currentColor);
                Console.Write(letter);

                if (!string.IsNullOrWhiteSpace(letter.ToString()))
                {
                    colorIndex++;
                    colorIndex %= 4;
                    currentColor = colors[colorIndex];
                }

            }

            ResetColor();
        }

        public static void WriteLine(object output = null, Color? fg = null)
        {
            Write(output, fg);
            Console.WriteLine();
        }

        public static void WriteWildLine(object output = null)
        {
            WriteWild(output);
            Console.WriteLine();
        }

        public static void WriteError(object output = null)
        {
            if (output == null)
            {
                return;
            }

            ChangeColor(ConsoleColor.White, ConsoleColor.Red);
            Console.Write(output);

            ResetColor();
        }

        public static void WriteErrorLine(object output = null)
        {
            WriteError(output);
            Console.WriteLine();
        }

        public static string ReadLine()
        {
            string input = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(input))
            {
                WriteErrorLine("Empty input. Please enter a response.");
                input = Console.ReadLine();
            }

            return input;
        }


        public delegate bool TryParse<T>(string input, out T result) where T : struct;
        public static T ReadLine<T>(TryParse<T> tryParse) where T : struct
        {
            T input;
            string inputStr = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(inputStr) || !tryParse(inputStr, out input))
            {
                WriteErrorLine($"Invalid input. Please enter a valid {typeof(T)}.");
                inputStr = Console.ReadLine();
            }

            return input;
        }

        public static int ReadIntInRange(int lower, int upper)
        {
            int input = ReadLine<int>(int.TryParse);
            while (input < lower || input > upper)
            {
                Console.WriteLine($"Input out of range. Please enter an integer between {lower}-{upper}.");
                input = ReadLine<int>(int.TryParse);
            }

            return input;
        }
    }
}
