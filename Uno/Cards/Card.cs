﻿using System;

namespace Uno.Cards
{
    public class Card : IComparable<Card>
    {
        public virtual Color Color {get; set;}
        public string Symbol {get; set;}

        public override string ToString()
        {
            return $"{Color} {Symbol}";
        }

        public Card(Color color, string symbol)
        {
            Color = color;
            Symbol = symbol;
        }

        public virtual bool CanBePlayedOn(Card other) => (Color == other.Color || Symbol == other.Symbol);

        public int CompareTo(Card rhs)
        {
            if (Color.CompareTo(rhs.Color) != 0)
            {
                return Color.CompareTo(rhs.Color);
            }
            else
            {
                bool lhsIsInt = int.TryParse(Symbol, out int lhsValue);
                bool yIsInt = int.TryParse(rhs.Symbol, out int yValue);

                if (lhsIsInt && yIsInt)
                {
                    return lhsValue.CompareTo(yValue);
                }
                else if (lhsIsInt && !yIsInt)
                {
                    return -1;
                }
                else if (!lhsIsInt && yIsInt)
                {
                    return 1;
                }
                else
                {
                    return Symbol.CompareTo(rhs.Symbol);
                }
            }
        }
    }
}
