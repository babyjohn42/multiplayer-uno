﻿using System;

namespace Uno.Cards
{
    public class WildCard : ActionCard
    {
        public WildCard() : base(Color.Wild, "Wild")
        {
        }

        public override string ToString()
        {
            string output = Symbol;
            if (Color != Color.Wild)
            {
                output += $" ({Color})";
            }

            return output;
        }

        public override bool CanBePlayedOn(Card other) => true;
        
        public override void EffectFromHand(GameManager manager)
        {
            UnoConsole.Write("Choose a color: ");
            Color newColor;

            while(!Enum.TryParse<Color>(Console.ReadLine(), out newColor) || newColor == Color.Wild)
            {
                UnoConsole.WriteLine("Please choose either Red, Blue, Green or Yellow.");
                UnoConsole.Write("Choose a color: ");
            }

            Color = newColor;
        }
    }
}
