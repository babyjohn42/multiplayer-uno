﻿namespace Uno.Cards
{
    public class SkipCard : ActionCard
    {
        public SkipCard(Color color) : base (color, "Skip")
        {
        }

        public override void EffectFromHand(GameManager manager)
        {
            manager.Skip();
        }
    }
}
