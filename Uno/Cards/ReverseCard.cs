﻿namespace Uno.Cards
{
    public class ReverseCard : ActionCard
    {
        public ReverseCard(Color color) : base(color, "Reverse")
        {
        }

        public override void EffectAsFirstCard(GameManager manager)
        {
            manager.CurrentPlayer = manager.Dealer;
            manager.ReverseOrder();
        }

        public override void EffectFromHand(GameManager manager)
        {
            manager.ReverseOrder();
        }
    }
}
