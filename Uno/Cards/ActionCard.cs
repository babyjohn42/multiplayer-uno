﻿namespace Uno.Cards
{
    public abstract class ActionCard : Card
    {
        public ActionCard(Color color, string symbol) : base(color, symbol)
        {
        }

        public abstract void EffectFromHand(GameManager manager);
        public virtual void EffectAsFirstCard(GameManager manager)
        {
            EffectFromHand(manager);
        }
    }
}
