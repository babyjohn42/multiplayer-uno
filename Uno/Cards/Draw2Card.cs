﻿namespace Uno.Cards
{
    public class Draw2Card : ActionCard
    {
        public Draw2Card(Color color) : base(color, "Draw 2")
        {
        }

        public override void EffectFromHand(GameManager manager)
        {
            manager.NextPlayerDraws(2);
        }
    }
}
