﻿namespace Uno.Cards
{
    public class WildDraw4Card : WildCard
    {
        public WildDraw4Card() : base()
        {
            Symbol = "Wild Draw 4";
        }

        public override void EffectAsFirstCard(GameManager manager)
        {
            manager.DiscardPile.Draw();
            manager.DrawPile.Discard(this);
            manager.DrawPile.Shuffle();

            Card topCard = manager.DrawPile.Draw();
            manager.DiscardPile.Discard(topCard);

            if (topCard is ActionCard actionCard)
            {
                actionCard.EffectAsFirstCard(manager);
            }
        }

        public override void EffectFromHand(GameManager manager)
        {
            base.EffectFromHand(manager);
            manager.NextPlayerDraws(4);
        }
    }
}
