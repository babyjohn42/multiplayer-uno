﻿using System;
using System.Collections.Generic;
using Uno.Cards;

namespace Uno
{
    public class GameInitializer
    {
        public void Initialize()
        {
            GameManager manager = new GameManager();

            InitializeDeck(manager);
            InitializePlayers(manager);
            ChooseDealer(manager);
            DealCards(manager);
            StartDiscardPile(manager);

            manager.EndTurn(true);
        }

        private void InitializePlayers(GameManager manager)
        {
            manager.Players = new List<Player>();

            UnoConsole.Write("How many players are playing? ");
            int numOfPlayers = UnoConsole.ReadIntInRange(2, 10);

            for (int i=0; i<numOfPlayers; i++)
            {   
                UnoConsole.Write($"Player {i+1}, please enter your name: ");
                try
                {
                    manager.Players.Add(new Player(UnoConsole.ReadLine()));
                }
                catch
                {
                    UnoConsole.WriteErrorLine("Invalid name.");
                    i--;
                }

            }
        }

        private void InitializeDeck(GameManager manager)
        {
            UnoDrawPileBuilder builder = new UnoDrawPileBuilder();
            builder.Build();
            manager.DrawPile = builder.Deck;
        }

        private void ChooseDealer(GameManager manager)
        {
            int index;
            if (manager.Dealer == null)
            {
                Random rng = new Random();
                index = rng.Next(0, manager.Players.Count);
            }
            else
            {
                index = manager.Players.IndexOf(manager.Dealer);
                index = (index + 1) % manager.Players.Count;
            }

            manager.Dealer = manager.Players[index];
            UnoConsole.WriteLine($"{manager.Dealer.Name} is the dealer!");
        }

        private void DealCards(GameManager manager)
        {
            int numOfPlayers = manager.Players.Count;
            manager.NextPlayer(false);
            
            for (int i=0; i<7; i++)
            {
                for (int j=0; j<numOfPlayers; j++)
                {
                    manager.PlayerDraws(1, false);
                    manager.NextPlayer(false);
                }
            }
        }


        public void StartDiscardPile(GameManager manager)
        {
            manager.CurrentPlayer = manager.Dealer;

            manager.DiscardPile = new Deck(true);
            Card topCard = manager.DrawPile.Draw();
            manager.DiscardPile.Discard(topCard);

            if (topCard is ActionCard actionCard)
            {
                actionCard.EffectAsFirstCard(manager);
            }
        }
    }
}
