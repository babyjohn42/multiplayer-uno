using System;
using System.Collections.Generic;
using Uno.Cards;

namespace Uno
{
    public class Deck
    {
        private Stack<Card> deck = new Stack<Card>();
     
        private static Random rng = new Random();

        public int Count {get => deck.Count;}

        public bool IsEmpty { get => deck.Count == 0; }
        
        public bool IsFaceUp {get; set;}

        public Deck(bool isFaceUp)
        {
            IsFaceUp = isFaceUp;
        }

        public Card Draw()
        {
            return deck.Pop();
        }

        public void Discard(Card card)
        {
            deck.Push(card);
        }

        public Card Peek()
        {
            return IsFaceUp ? deck.Peek() : null;
        }

        public void Shuffle()
        {
            Card[] cardArray = deck.ToArray();
            
            // Fisher-Yates shuffle
            for (int n = cardArray.Length - 1; n > 0; --n)
            {
                int k = rng.Next(n + 1);
                Card tmp = cardArray[n];
                cardArray[n] = cardArray[k];
                cardArray[k] = tmp;
            }

            // reset any wild cards
            foreach (Card card in cardArray)
            {
                if (card is WildCard wildCard)
                {
                    wildCard.Color = Color.Wild;
                }
            }

            deck = new Stack<Card>(cardArray);
        }
    }
}
