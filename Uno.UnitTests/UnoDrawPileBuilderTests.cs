﻿using NUnit.Framework;

namespace Uno.UnitTests
{
    [TestFixture]
    public class UnoDrawPileBuilderTests
    {
        [Test]
        public void Build_Always_Adds108Cards()
        {
            // Arrange
            UnoDrawPileBuilder builder = new UnoDrawPileBuilder();

            // Act
            builder.Build();

            // Assert
            Assert.AreEqual(builder.Deck.Count, 108);
        }

        [Test]
        public void Deck_IsNull_BeforeCallingBuild()
        {
            // Arrange
            UnoDrawPileBuilder builder = new UnoDrawPileBuilder();

            // Act
            // Assert
            Assert.IsNull(builder.Deck);
        }

        [Test]
        public void Deck_IsNotNull_AfterCallingBuild()
        {
            // Arrange
            UnoDrawPileBuilder builder = new UnoDrawPileBuilder();

            // Act
            builder.Build();

            // Assert
            Assert.IsNotNull(builder.Deck);
        }
    }
}
